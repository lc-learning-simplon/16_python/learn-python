# Exercices

# Adapter le code précédent pour afficher tous les nombres de 1 à 100.
for i in range(101):
    print(i)

# Afficher un rectangle de taille 4x5 composé de "x".
print("XXXX\n" * 5)

# Afficher tous les nombres de à 100 à 1.
for i in range(100):
    print(100-i)

# Afficher un triangle composé de 10 "o" au total.
print("o\noo\nooo\noooo")

# Écrire un programme qui demande 5 prénoms, les stocke dans un tableau, puis les affiche.
# names = []
# for i in range(5):
#     names.append(input("Entrez un prénom : "))
# print(names)

# Compléter ce code pour afficher les films diffusés après 12h :
horaires = [
    {'film': 'Seul sur Mars', 'heure': 9},
    {'film': 'Astérix et Obélix Mission Cléopâtre', 'heure': 10},
    {'film': 'Star Wars VII', 'heure': 15},
    {'film': 'Time Lapse', 'heure': 18},
    {'film': 'Fatal', 'heure': 20},
    {'film': 'Limitless', 'heure': 20},
]

for film in horaires:
    if film["heure"] >= 15:
        print(film["film"])